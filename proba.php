<?php
$summa = $argv[1];
$input = $argv[2];
$output = $argv[3];
$response = '';
$kursJson = downLoadJsonFromApi();
$kursArr = json_decode($kursJson);
$kurs = '';
// преобразовал обьект в массив
$newKurs = [];
foreach ($kursArr as $value) {
    $newKurs[] = (array)$value;
}

/*
[{
    "ccy":"USD","base_ccy":"UAH","buy":"28.20000","sale":"28.60000"},{
    "ccy":"EUR","base_ccy":"UAH","buy":"32.85000","sale":"33.45000"},{
    "ccy":"RUR","base_ccy":"UAH","buy":"0.34900","sale":"0.37700"},{
    "ccy":"BTC","base_ccy":"USD","buy":"10151.2882","sale":"11219.8448"}
]
*/

$response = null;
foreach ($newKurs as $item) {
    if ($input == $item['ccy'] && $output == $item['base_ccy']) {
        $kurs = $item['buy'];
        $response = $summa * $kurs;
    } else if ($input == $item['base_ccy'] && $output == $item['ccy']) {
        $kurs = $item['sale'];
        $response = $summa / $kurs;
    }
}

if (is_null($response)) {
    echo "Нету курса валют по переводу {$input} в {$output}\n";
} else {
    echo "{$response}\n";
}

function downLoadJsonFromApi()
{
    $date = date('Ymd_H');
    $filePathCache = __DIR__ . "/cache-{$date}.json";

    if (file_exists($filePathCache)) {
        $contents = file_get_contents($filePathCache);
    } else {
        $contents = file_get_contents('https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5');
        file_put_contents($filePathCache, $contents);
    }

    return $contents;
}