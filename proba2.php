<?php
class Point
{
    public function __construct($x, $y) {
        $this->x = $x;
        $this->y = $y;
    }

    public $x = 3;
    public $y = 5;
}

$badArr = [
    new Point(10, 100),
    new Point(20, 200),
];
$newArr = [];
foreach ($badArr as $value)
{
    $newArr[] = (array) $value;
}

var_dump($newArr);