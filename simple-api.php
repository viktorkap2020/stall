<?php
// АПИ для получения курса валют. Умеет USD и EUR

$courses = [
    'usd' => 28,
    'eur' => 33,
];

$currency = $_GET['currency'];
$course = $courses[$currency] ?? null;
$success = !is_null($course);

echo json_encode(['success' => $success, 'course' => $course]);
