<?php
$arr = [
    [
        'first_name' => 'Іра',
        'last_name'  => 'Ворза',
        'age'        => 35,
    ],
    [
        'first_name' => 'Петро',
        'last_name'  => 'Мазур',
        'age'        => 21,
    ],
    [
        'first_name' => 'Сергій',
        'last_name'  => 'Юхимов',
        'age'        => 31,
    ],

    [
        'first_name' => 'Ігор',
        'last_name'  => 'Добрий',
        'age'        => 22,
    ],
    [
        'first_name' => 'Олег',
        'last_name'  => 'Пархонов',
        'age'        => 20,
    ],
    [
        'first_name' => 'Станіслав',
        'last_name'  => 'Гетьман',
        'age'        => 33,
    ],
];

class SQL
{
    public $table = [];

    public function __construct()
    {
        $table = $this->table;
    }

    public function arr_to_table(array $arr)
    {
        $this->table = $arr;
    }

    public function run($sql)
    {
        $sql = str_replace("SELECT ", '', $sql);
        $fromParts = explode(" FROM ", $sql);

        // TODO: обработать случай, когда полей много
        $field = $fromParts[0];
        $table = $fromParts[1];

        if ($table !== 'table_1') {
            throw new Exception("Bad table name!");
        }



        $result = [];
        foreach ($this->table as $row)
        {
             // $row = ['first_name' => 'Станіслав', 'last_name' => 'Гетьман', 'age' => 33]
            // $field = "last_name, first_name"
            $key = explode(",", $field);
            var_dump($key);
            foreach ($key as $rows){
            if (isset($row[$key])) {


                    $result[] = $row[$rows];
                }

                $resultRow = [$field => $row[$field]];
                $result[] = $resultRow;
            }
        }

        return $result;
    }
}

$table = new SQL();
$table->arr_to_table($arr);
$result = $table->run("SELECT last_name, first_name FROM table_1");

//var_dump($result);
